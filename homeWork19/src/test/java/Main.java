import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Alert;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Main {
    WebDriver driver;

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.gecko.driver",
                "/Users/macbookpro/Documents/Hillel_QA_Manual/HomeWorks_Java/Drivers/geckodriver");

    }
    @Before
    public void precondition(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void openUserData() throws InterruptedException {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys("Vlad", Keys.TAB);
        driver.findElement(By.cssSelector("#last_name")).sendKeys("Buiukli", Keys.TAB);
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys("7188338", Keys.TAB);
        driver.findElement(By.cssSelector("#field_phone")).sendKeys("380930847824", Keys.TAB);
        driver.findElement(By.cssSelector("#field_email")).sendKeys("Bujukli@gmail.com", Keys.TAB);
        driver.findElement(By.cssSelector("#field_password")).sendKeys("79492957Vlad", Keys.TAB);
        driver.findElement(By.cssSelector("#male")).click();
        driver.findElement(By.cssSelector("#position")).sendKeys("qa", Keys.TAB);
        driver.findElement(By.cssSelector("#button_account")).click();

        Alert alert = driver.switchTo().alert();
        alert.accept();
        Thread.sleep(3000);

        driver.navigate().refresh();
        driver.findElement(By.cssSelector(".email")).sendKeys("Bujukli@gmail.com", Keys.TAB);
        driver.findElement(By.cssSelector(".password")).sendKeys("79492957Vlad", Keys.TAB);
        driver.findElement(By.cssSelector(".login_button")).click();
        driver.findElement(By.cssSelector("#employees")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys("Vlad", Keys.TAB);
        driver.findElement(By.cssSelector("#last_name")).sendKeys("Buiukli", Keys.TAB);
        driver.findElement(By.cssSelector("#search")).click();

    }

    @After
    public void postcondition(){
        driver.quit();
    }
}
